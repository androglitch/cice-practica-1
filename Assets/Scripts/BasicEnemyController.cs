﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BasicEnemyController : MonoBehaviour
{

    /**
     * BasicEnemyController
     * 
     * Script de control de los enemigos básicos
     * Variables:
     *     halo: Objeto de tipo Light que se utilizará para controlar el color de la luz.
     *     bullet: Prefab de la bala que los enemigos dispararán.
     *     colorNormal: Color el enemigo en estado normal.
     *     colorPlayer: Color del enemigo cuando detecta al jugador.
     *     velocity: Velocidad con la que el enemigo se moverá hacia el jugador.
     *     timeToShoot: Tiempo entre disparos, si no se indica en Unity se asignará 5.
     *     time: Tiempo transcurrido desde el último disparo, se resetea a 0.
     *     initialPosition: Posición inicial del enemigo, sirve para que el enemigo pueda respawnear
     *     player: Componente Transform del jugador, se utilizará moverse hacia él.
     *     rb: Rigidbody del enemigo.
     *     pause: Variable que indica si el enemigo debe permanecer en estado de pausa o no.
     */

    public Light halo;
    public GameObject bullet;
    public Color colorNormal;
    public Color colorPlayer;
    public float velocity;
    public float timeToShoot=5;
    public float time;
    Vector3 initialPosition;
    Transform player;
    Rigidbody rb;
    bool pause = false;

    // Se recogen todas las variables privadas que serán necesarias para el funcionamiento del Script.
    void Start(){
        rb = GetComponent<Rigidbody>();
        initialPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if(!pause)
            MoveToPlayer();
    }

    private void Update()
    {
        if(!pause)
            Shoot();
    }
    // Función encargada de disparar al jugador
    void Shoot() {
        if (player != null) { //Evalua si el jugador se ha detectado o no.
            time += Time.deltaTime; //Contador de tiempo.
            if (time >= timeToShoot) {
                time = 0;
                GameObject _bullet = Instantiate(bullet, gameObject.transform.position, Quaternion.identity);
                //Se instancia la bala, se le indica que debe moverse hacia el jugador y se activa su movimiento.
                EnemyFireController _ctrl = _bullet.GetComponent<EnemyFireController>();
                _ctrl.SetDestination(player.position);
                _ctrl.Move();
            }
        }
    }
    //Función encargada de mover al enemigo hacia el jugador.
    void MoveToPlayer() {
        if (player != null) { //Evalua si el jugador se ha detectado o no.
            transform.LookAt(player); //Mira hacia el jugador
            //Se calcula el vector de la velocidad en el eje horizontal con el que el enemigo se moverá
            float x = transform.position.x + transform.forward.x * velocity * Time.fixedDeltaTime;
            float z = transform.position.z + transform.forward.z * velocity * Time.fixedDeltaTime;
            Vector3 finalPoint = new Vector3(x,transform.position.y,z);
            rb.MovePosition(finalPoint);
        }
    }
    void Color(Color color) {
        halo.color = color;
    }
    public void SetPause(bool _pause) {
        pause = _pause;
    }
    //Los enemigos desaparecen con un sólo golpe
    public void Hit() {
        StartCoroutine(RestartEnemy());
    }
    //Función conectada a la zona de detección del jugador, ésta cambia el color y resetea los parámetros de disparo
    public void PlayerDetected(GameObject _player)
    {
        time = 0;
        player = _player.transform;
        Color(colorPlayer);
    }
    //Función conectada a la zona de detección del jugador, devuelve al enemigo a su estado inicial.
    public void PlayerUndetected()
    {
        Color(colorNormal);
        player = null;
    }
    //Rutina encargada de volver a hacer el spawn del enemigo en su posición inicial transcurridos 5 segundos
    IEnumerator RestartEnemy()
    {
        transform.position = new Vector3(initialPosition.x,200.0f,initialPosition.z);
        yield return new WaitForSeconds(5);
        transform.position = initialPosition;
    }
}
