﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoController : MonoBehaviour{

    /**
     * AmmoController
     * Variables:
     *     ammo: Cantidad de munición que se añadirá al arma una vez recogida la caja de munición.
     */
    public int ammo;
}
