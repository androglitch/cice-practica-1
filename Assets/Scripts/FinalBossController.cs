﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBossController : MonoBehaviour
{

    /**
     * FinalBossController
     *
     * Script de control del jefe final.
     * Variables:
     *     dead: Indica si el boss está muerto o no (finalmente no se utiliza)
     *     player: GameObject del jugador, se utilizará para dirigirse a el y apuntarle.
     *     bullet: Prefab de las balas del enemigo.
     *     child: GameObject del jóven secuestrado al que hay que rescatar, se utiliza para 
     *            activar el objeto una vez el jefe final muere
     *     bulletVelocity: Velocidad de la bala.
     *     velocity: Velocidad con la que el enemigo se mueve hacia el jugador.
     *     timeToShoot: Tiempo entre disparos.
     *     timeToDie: Tiempo que el jugador deberá esperar antes de que el jefe 
     *                final muera en su último punto de vida.
     *     time: Contador de tiempo entre disparos.
     *     timeToDieCounter: Contador de tiempo hasta morir.
     *     life: Puntos de vida del jefe.
     *     mng: Controlador del manager de la escena completa, se utilizará para indicar
     *          si se alcanza el final bueno o el malo.
     *     rb: Rigidbody del objeto.
     *     firstHalo: Color del halo interior del enemigo.
     *     secondHalo: Color del halo exterior del enemigo.
     *     pause: Variable de pausa
     */

    bool dead = false;
    public GameObject player;
    public GameObject manager;
    public GameObject bullet;
    public GameObject child;

    float bulletVelocity = 1.5f;
    public float velocity;
    public float timeToShoot = 2;
    public float timeToDie = 30;
    public float time;
    public float timeToDieCounter;
    public int life = 10;
    ManagerController mng;
    Rigidbody rb;
    public Light firstHalo,secondHalo;
    bool pause;


    //Se recogen las variables iniciales que se utilizarán más adelante
    void Start()
    {
        mng = manager.GetComponent<ManagerController>();
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (!pause)
            MoveToPlayer();
    }

    private void Update()
    {
        if (!pause) {
            Shoot();
            if (life == 1)
                LastHit();
        }
    }
    //Sistema de disparo hacia el jugador
    void Shoot()
    {
        if (player != null) //Seguridad en caso de que no se asigne jugador
        {
            time += Time.deltaTime;
            if (time >= timeToShoot)
            {
                time = 0;
                GameObject _bullet = Instantiate(bullet, gameObject.transform.position, Quaternion.identity);
                EnemyFireController _ctrl = _bullet.GetComponent<EnemyFireController>();
                //Indicamos a la bala el punto de destino
                _ctrl.SetDestination(player.transform.position);
                //El jefe final dispara más rápido
                _ctrl.SetVelocity(bulletVelocity);
                _ctrl.Move();
            }
        }
    }
    //Movimiento hacia el jugador
    void MoveToPlayer()
    {
        if (player != null)
        {
            //Se mira al jugador.
            transform.LookAt(player.transform.position);
            //Se calcula el vector de dirección
            float x = transform.position.x + transform.forward.x * velocity * Time.fixedDeltaTime;
            float z = transform.position.z + transform.forward.z * velocity * Time.fixedDeltaTime;
            Vector3 finalPoint = new Vector3(x, transform.position.y, z);
            rb.MovePosition(finalPoint);
        }
    }
    //Función de control del último golpe.
    void LastHit() {
        if (life == 1)
        {
            timeToDieCounter += Time.deltaTime;
            if (timeToDieCounter >= timeToDie) {
                life--;
                dead = true;
                //Desactiva al jefe final
                gameObject.SetActive(false);
                //Activa al jóven a rescatar
                child.SetActive(true);
            }
        }
    }
    //Función de recepción de golpes, se invocará desde el script del jugador cada
    //vez que éste dispare y el enemigo se encuentre a tiro.
    public void Hit()
    {
        life--;
        //En el último punto el jefe dispara cada menos tiempo y más rápido,
        //Cambia de color para indicar que es su último punto.
        if (life == 1) {
            timeToShoot /= 2;
            bulletVelocity *= 2;
            firstHalo.color = Color.blue;
            secondHalo.color = Color.yellow;
        }
        //Si el jugador dispara cuando el jefe está en su último punto de vida, se
        //desbloquea el final malo.
        if (life == 0){
            mng.BadEnding();
            dead = true;
        }
    }
    //Evaluación externa para saber si el jefe final está muerto o no.
    public bool IsDead() {
        return dead;
    }
    //Función que se llamará desde el ManagerController para poner al enemigo en
    //estado pausa.
    public void SetPause(bool _pause)
    {
        pause = _pause;
    }
}
