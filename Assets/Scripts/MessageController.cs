﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageController : MonoBehaviour
{

    /**
     * MessageController
     * 
     * Script que estará asignado a todos los objetos que tienen que mostrar un mensaje contextual
     * 
     * Variables:
     *     message: Asignado desde Unity, contiene el mensaje que se mostrará sobre el papel.
     *     messageBox: GameObject que contiene el "papel" con el mensaje dentro.
     *     textMessage: Componente de texto de la UI que mostrará el mensaje
     *
     */
    public string message;
    public GameObject messageBox;
    public Text textMessage;


    public void ShowText() {
        textMessage.text = message;
        messageBox.SetActive(true);
    }
    public void HideText()
    {
        messageBox.SetActive(false);
    }
}
