﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    /**
     * PlayerController
     * 
     * Script encargado de implementar el movimiento, las mecánicas, los estados
     * y de activar los PowerUps del jugador
     * 
     * Variables:
     *     camera: Componente Transform de la cámara, se utilizará para dirigir la
     *             visión del jugador y para apuntar el Raycast
     *     light: GameObject del objeto de luz, se utilizará para activar/desactivar la linterna
     *     bengal: prefab de las bengalas.
     *     manager: Objeto manager global de la escena.
     *     context: Objeto de texto de la UI.
     *     pause: Booleana que indica si el jugador está en estado de pausa o no.
     *     guns: número de pistolas que tiene el jugador, el Power Up aumenta en 1 este número
     *     lifes: número de golpes que el jugador puede recibir.
     *     bengals: número de bengalas que el jugador podrá usar para orientarse.
     *     delayingFire: booleana que sirve para retrasar el disparo.
     *     delayFire: Tiempo de retardo entre disparos, con doble pistola es la mitad.
     *     time: Contador de tiempo entre disparos.
     *     maxRunTime: Tiempo máximo que el jugador podrá correr.
     *     runingTime: Contador de tiempo de carrera.
     *     tired: Booleana que indica si el jugador está cansado o no.
     *     gravity: Gravedad que afectará al CharacterController.
     *     speed: velocidad con la que el jugador se moverá en condiciones normales.
     *     jumpHeight: Altura máxima de salto
     *     hSensibility: Sensibilidad horizontal el ratón.
     *     vSensibility: Sensibilidad verticaldel ratón
     *     messageDistance: Distancia a la que el jugador debe estar de un mensaje
     *                      para que se muestre.
     *     enemyDistance: Distancia a la que el jugador debe estar de un enemigo
     *                    para poder dispararle.
     *     velocity: Variable privada que contendrá el vector de dirección calculado.
     *     cc: Componente CharacterController
     *     MessageController: Objeto que contendrá el mensaje al que se está apuntando.
     *     BasicEnemyController: Objeto que contendrá al enemigo al que se está apuntando.
     *     FinalBossController: Objeto que contendrá al jefe final si se le apunta.
     *     ManagerController: Objeto que contiene al manager global de la escena.
     */

    public Transform camera;
    public GameObject light;
    public GameObject bengal;
    public GameObject manager;
    public Text context;

    bool pause = false;
    int guns = 1;
    int lifes = 3;
    int bengals = 10;
    int ammo = 20;
    bool delayingFire = false;
    float delayFire = 1.0f;
    float time = 0.0f;
    float maxRunTime = 5.0f;
    public float runingTime = 0;
    public bool tired = false;
    float gravity = -20f;
    float speed = 6.0f;
    float jumpHeight = 2.0f;
    float hSensibility = 360;
    float vSensibility = 50;
    float messageDistance = 2.5f;
    float enemyDistance = 20f;
    Vector3 velocity;
    CharacterController cc;
    MessageController messageBox;
    BasicEnemyController enemy;
    FinalBossController finalBoss;
    ManagerController mng;

    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
        mng = manager.GetComponent<ManagerController>();
    }

    //Se crea un rayo que se evaluará más adelante
    private void FixedUpdate()
    {
        if (!pause)
        {
            Ray front = new Ray(camera.transform.position, camera.transform.forward.normalized);
            Message(front);
            Enemy(front);
        }
    }
    //Gestión de los eventos relacionados con el movimiento, disparos, luces y bengalas
    void Update(){
        if (!pause)
        {
            Movement();
            HorizontalView();
            VerticalView();
            KillEnemy();
            PlaceBengal();
            SwitchLight();
        }
    }
    //Trigger que se encarga de detectar los objetos que chocan con el jugador
    private void OnTriggerEnter(Collider other)
    {
        //Detecta la capa de muerte instantanea
        if (other.gameObject.layer == 9)
        {
            Killed();
        }
        //Deteca que el jugador ha tocado una caja de munición
        if (other.tag == "Ammo")
        {
            AmmoController ctrl = other.GetComponent<AmmoController>();
            //Se suma la munición que la caja indique
            ammo += ctrl.ammo;
            UpdateContext();
            other.gameObject.SetActive(false);
        }
        //Detecta que el jugador ha chocaco con una caja de vida (finalmente no se usa)
        if (other.tag == "Life")
        {
            lifes++;
            UpdateContext();
            other.gameObject.SetActive(false);
        }
        //Detecta que el jugador ha alzandado el PowerUp de segunda pistola
        if (other.tag == "Gun")
        {
            //Como medida de seguridad en lugar de incrementar se iguala a 2 ya que
            //el jugador ya portaba una y no puede utilizar 3.
            guns = 2; 
            UpdateContext();
            other.gameObject.SetActive(false);
        }
        //Detecta que el jugador es alcanzado por fuego enemigo
        if (other.tag == "EnemyFire")
        {
            other.gameObject.SetActive(false);
            lifes--;
            UpdateContext();
            if (lifes == 0) Killed();
        }
        //Detecta que el jugador ha contactado con el joven desaparecido
        if (other.tag == "Child")
        {
            mng.GoodEnding();
        }
    }
    //Evalua si el objeto con el que choca el objeto se encuentra dentro de la distancia
    //en la que los mensajes pueden ser leidos y si el objeto se encuentra en la capa de
    //mensajes.
    void Message(Ray front) {
        RaycastHit other;
        if (Physics.Raycast(front, out other, messageDistance))
        {
            if (other.collider.gameObject.layer == 8)
            {
                messageBox = other.collider.gameObject.GetComponent<MessageController>();
                messageBox.ShowText();
            }
            else if (messageBox != null) //Se oculta el mensaje por dejar de apuntar.
                    messageBox.HideText();
        }
        else if (messageBox != null) //Se oculta el mensaje por distancia.
                messageBox.HideText();
    }
    //Evalua si el objeto con el que choca el objeto se encuentra dentro de la distancia
    //en la que los enemigos pueden ser atacados y si el objeto se trata de un enemigo
    //o del jefe final
    void Enemy(Ray front)
    {
        RaycastHit other;
        if (Physics.Raycast(front, out other, enemyDistance))
        {
            if (other.collider.gameObject.tag == "Enemy")
            {
                enemy = other.collider.gameObject.GetComponent<BasicEnemyController>();
            }
            else if (other.collider.gameObject.tag == "FinalBoss")
            {
                finalBoss = other.collider.gameObject.GetComponent<FinalBossController>();
            }
            else if (enemy != null)
            {
                //Se resetea por dejar de apuntar.
                enemy = null;
                finalBoss = null;
            }
        }
        else if (enemy != null)
        {
            //Se resetea por distancia.
            enemy = null;
            finalBoss = null;
        }
    }
    //Función que se invoca cuando se tiene un enemigo a tiro se pulsa click izquierdo.
    void KillEnemy() {
        if (!delayingFire)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (ammo > 0)
                {
                    delayingFire = true;
                    time = 0;
                    if (enemy != null)
                    {
                        enemy.Hit();
                        enemy = null;
                    }
                    else if (finalBoss != null) {
                        finalBoss.Hit();
                    }
                    ammo--;
                    UpdateContext();
                }
            }
        }
        else {
            time += Time.deltaTime;
            float delayTime = (light.activeSelf) ? delayFire : delayFire / guns;
            if (time >= delayTime) {
                delayingFire = false;
                time = 0;
            }
        }
    }
    //Coloca una bengala en la posición en la que se encuentra el jugador.
    void PlaceBengal() {
        if (bengals > 0) {
            if (Input.GetKeyDown(KeyCode.E)) {
                Instantiate(bengal,gameObject.transform.position+gameObject.transform.forward,Quaternion.identity);
                bengals--;
                UpdateContext();
            }
        }
    }
    //Movimiento del jugador
    void Movement()
    {
        float localSpeed = speed;
        if (cc.isGrounded && velocity.y < 0) velocity.y = -2f;
        //Mecánica de salto
        if (cc.isGrounded && Input.GetButtonDown("Jump"))
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        //Mecánica de movimiento horizontal
        Vector3 move = transform.right * x + transform.forward * z;
        //Para determinar la velocidad del movimiento evaluamos si el jugador está
        //cansado o no
        if (!tired)
        {
            //Mecánica de correr, el jugador corre 3 veces más rápido
            if (Input.GetKey(KeyCode.LeftShift) && move!=Vector3.zero)
            {
                localSpeed *= 3;
                runingTime += Time.deltaTime;
                tired = (runingTime >= maxRunTime);
            }
            else if (runingTime > 0) runingTime -= Time.deltaTime;
        }
        else {
            localSpeed = speed * 0.5f;
            runingTime -= Time.deltaTime;
            tired = (runingTime > 0);
        }
        //Se aplican los movimientos horizontales y verticales
        cc.Move(move * localSpeed * Time.deltaTime);
        velocity.y += gravity * Time.deltaTime;
        cc.Move(velocity * Time.deltaTime);
    }

    //Movimiento horizontal del ratón
    void HorizontalView()
    {
        float mouseX = ((Input.mousePosition.x / Screen.width) - 0.5f) * hSensibility;
        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, mouseX, transform.localRotation.z));
    }
    //Movimiento vertical del ratón
    void VerticalView() {
        float mouseY = ((Input.mousePosition.y / Screen.height) - 0.5f) * vSensibility;
        //Se evita que se pueda dar un giro de 360º en sentido vertical.
        mouseY = Mathf.Clamp(mouseY, -90, 90);
        camera.localRotation = Quaternion.Euler(new Vector3(mouseY, camera.transform.localRotation.y, camera.transform.localRotation.z));
    }
    //Cambio de estado de la luz (encendido/apagado).
    void SwitchLight() {
        if (Input.GetKeyDown(KeyCode.F)) {
            light.SetActive(!light.activeSelf);
        }
    }
    //Actualiza la información contextual del jugador.
    void UpdateContext() {
        string message = "Vidas: "+lifes+" | Balas: "+ammo+ " | Bengalas: "+bengals;
        if (guns > 1) message += " | Armas: "+guns;
        mng.SetText(message);
        
    }
    //Invocación cuando el jugador muere.
    void Killed() {
        mng.GameOver();
        
    }
    //Establece el estado de pausa, si el estado de pausa es false vuelve a mostrar
    // la información contextual del jugador.
    public void SetPause(bool _pause)
    {
        pause = _pause;
        if (!pause) {
            UpdateContext();
        }
    }
}
