﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinEffect : MonoBehaviour
{

    /**
     * SpinEffect
     * 
     * Script que se encarga de hacer girar objetos con el fin de destacarlos.
     * Variables:
     *     omega: Velocidad de rotación.
     */

    public float omega = 4;
    
    private void Update(){
        transform.Rotate(Vector3.up,omega,Space.Self);
    }
}
