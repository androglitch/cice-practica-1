﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBossWallController : MonoBehaviour
{

    /**
     * FinalBossWallController
     * 
     * Script que se encarga de activar el muro invisible que evitará que el jugador huya
     * y también activará al enemigo para que comience la lucha final
     * 
     * Variables:
     *     wall: Objeto del muro invisible.
     *     boss: Objecto del jefe final.
     */
    public GameObject wall;
    public GameObject boss;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player"){
            boss.SetActive(true);
            wall.SetActive(true);
        }
    }
}
