﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFireController : MonoBehaviour
{
    /**
     * 
     * EnemyFireController
     * 
     * Script de control del comportamiento de la bala de los enemigos, para hacer posible para el jugador
     * esquivarlas, estas se manejan mediante físicas.
     * 
     * Variables:
     *     rb: El Rigidbody de la bala en cuestión sobre la que se aplicarán las fuerzas.
     *     move: Variable booleana que indica si la bala se puede mover o no.
     *     pause: Variable de pausa
     *     direction: Variable que almacena la dirección hacia la que se moverá la bala.
     *     velocity: Velocidad de movimiento de la bal no se indica nada se igualará a 1.
     * 
     */

    Rigidbody rb;
    bool move = false;
    bool pause = false;
    Vector3 direction;
    float velocity = 1f;

    //Obtención del Rigidbody
    private void Start() {
        rb = GetComponent<Rigidbody>();
    }
    //Una vez la bala se puede mover si aplica la velocidad en la dirección calculada.
    private void FixedUpdate()
    {
        if (move && !pause){
            rb.AddForce(direction * velocity, ForceMode.VelocityChange);
        }
    }
    //Cuando se le indica la posición del jugador, se calcula la dirección de movimiento de la bala
    public void SetDestination(Vector3 _final) {
        direction = _final - gameObject.transform.position;
        direction.Normalize();
    }
    //Mediante esta función pública se puede modificar la velocidad de la bala.
    public void SetVelocity(float _velocity) {
        velocity = _velocity;
    }
    //Función pública para indicar a la bala que ya está configurada y que se puede mover.
    public void Move() {
        move = true;
        Destroy(gameObject, 3);
    }
}
