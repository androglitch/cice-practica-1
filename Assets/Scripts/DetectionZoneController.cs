﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionZoneController : MonoBehaviour
{
    /**
     * 
     * DetectionZoneController
     * 
     * Script que se encarga de detectar al jugador.
     * Variables:
     *     main: Conecta con el enemigo y le indica mediante sus métodos públicos 
     *           si el jugador se encuentra dentro de la zona de acción o no.
     * 
     */
    public BasicEnemyController main;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            main.PlayerDetected(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            main.PlayerUndetected();
        }
    }
}
