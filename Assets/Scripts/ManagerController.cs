﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManagerController : MonoBehaviour
{

    /**
     * ManagerController
     * 
     * Controlador general de la escena, se encarga de la UI y de los estados
     * de pausa y Game Over.
     * 
     * Variables:
     *     pausableObjects: Array que contiene los objetos que serán pausables en la escena.
     *     pause: Variable que indica si la escena está pausada o no.
     *     gameOver: Variable que indica si el juego terminó o no.
     *     context: Variable que contiene el objeto que muestra el texto en la parte superior
     *              izquierda de la pantalla.
     */
    public List<GameObject> pausableObjects;
    bool pause = false;
    bool gameOver = false;
    public Text context;

    // Update is called once per frame
    void Update()
    {
        //La tecla asignada al pause será el escape, cuando se pulse el
        //estado de pausa cambiará de verdadero a falso
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pause = !pause;
            //Si corresponde se indica al jugador que está en pausa
            if (pause) context.text = "Pausa";
            SetPauseList();
        }
        //La tecla asignada a volver a jugar es el espacio, si el juego terminó
        //la escena se reinicia pulsando espacio.
        if (gameOver && Input.GetKeyDown(KeyCode.Space))
            SceneManager.LoadScene(0);
    }
    //Función pública para indicar que el juego terminó, cambia el texto superior.
    public void GameOver() {
        context.text = "Has Muerto. Pulsa espacio para comenzar de nuevo";
        pause = true;
        gameOver = true;
        SetPauseList();
    }
    //Función pública para cambiar el texto superior.
    public void SetText(string msg) {
        context.text = msg;
    }
    //Función pública invocada desde el objeto del joven a rescatar para indicar
    //que se alcancó el final bueno.
    public void GoodEnding() {
        pause = true;
        gameOver = true;
        context.text = "Enhorabuena, has ganado. Pulsa espacio para comenzar de nuevo";
        SetPauseList();
    }
    //Función pública invocada desde el objeto del jefe final para indicar
    //que se alcancó el final malo.
    public void BadEnding() {
        pause = true;
        gameOver = true;
        context.text = "Has Muerto. Pulsa espacio para comenzar de nuevo";
        SetPauseList();
    }
    //Recorre el array de objetos pausables y llama a su función SetPause para
    //que éstos transicionen en el estado correcto.
    void SetPauseList() {
        for (int i = 0; i < pausableObjects.Count; i++)
        {

            PlayerController pc = pausableObjects[i].GetComponent<PlayerController>();
            if (pc != null) pc.SetPause(pause);
            else
            {
                BasicEnemyController ec = pausableObjects[i].GetComponent<BasicEnemyController>();
                if (ec != null)
                    ec.SetPause(pause);
                else
                {
                    FinalBossController fc = pausableObjects[i].GetComponent<FinalBossController>();
                    if (fc != null)
                        fc.SetPause(pause);
                }
            }
        }
    }
}
